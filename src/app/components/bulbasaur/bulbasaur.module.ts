import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BulbasaurComponent } from './bulbasaur.component';


@NgModule({
	declarations: [
		BulbasaurComponent
	],
	exports: [
		BulbasaurComponent
	],
	imports: [
		CommonModule
	]
})
export class BulbasaurModule {
}
