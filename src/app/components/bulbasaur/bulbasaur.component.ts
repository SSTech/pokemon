import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-component-bulbasaur',
	templateUrl: './bulbasaur.component.html',
	styleUrls: [ './bulbasaur.component.scss' ]
})
export class BulbasaurComponent implements OnInit {

	@Input()
	public title!: string;

	@Input()
	public description!: string;

	@Input()
	public image!: string;

	@Input()
	public backgroundImage?: string;

	@Input()
	public number!: string;

	constructor() {
	}

	ngOnInit(): void {
	}

}
