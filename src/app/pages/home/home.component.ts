import { Component, OnInit } from '@angular/core';
import { BulbasaurInterface } from './interface/bulbasaur.interface';

@Component({
	selector: 'app-page-home',
	templateUrl: './home.component.html',
	styleUrls: [ './home.component.scss' ]
})
export class HomeComponent implements OnInit {

	public bulbasaurs: BulbasaurInterface[] = [];

	constructor() {
		for (let i = 0; i < 10; i++) {
			this.bulbasaurs.push({
				number: '#001',
				description: 'There is a plant seed on its back right from the day this Pokémon is born. The seed slowly grows larger.',
				title: 'Bulbasaur',
				image: '../../../assets/images/Bulbasaur.png',
				backgroundImage: '../../../assets/images/CircleBackground.png',
			});
		}
	}

	ngOnInit(): void {
	}

}
