export interface BulbasaurInterface {
	image: string;

	title: string;

	description: string;

	number: string;

	backgroundImage?: string;
}